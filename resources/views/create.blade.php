@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">Create User</div>

                    <table class="table" id="userInputForm">
                        <tr>
                            <td>
                                Name :
                            </td>
                            <td>
                                <input type="text" v-model="name"/>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Email :
                            </td>
                            <td>
                                <input type="email" v-model="email"/>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Phone :
                            </td>
                            <td>
                                <input type="text" v-model="phone"/>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Display Picture :
                            </td>
                            <td>
                                <input type="file" id="picture" accept="image/*" v-on:change="onFileChange"/>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Interests :
                            </td>
                            <td>
                                <ul style="list-style: none">
                                    @foreach($interests as $interest)
                                        <li>
                                            <input type="checkbox"
                                                   id="{{$interest->name}}"
                                                   value="{{$interest->id}}"
                                                   v-model="interests">
                                            <label for="{{$interest->name}}">{{$interest->name}}</label>
                                        </li>
                                    @endforeach
                                </ul>
                            </td>
                        </tr>
                        <tr>
                            <td rowspan="2">
                                <button v-on:click.stop.prevent="createUser" class="button">Create User!</button>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </div>

@endsection
