@extends('layouts.app')

@section('content')

    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">Users</div>

                    <table class="table">
                        <tr v-for="user in users" >
                            <td>
                                Name:
                            </td>
                            <td>
                                @{{user.name}}
                            </td>
                            <td>
                                Email:
                            </td>
                            <td>
                                @{{user.email}}
                            </td>
                            <td>
                                Phone:
                            </td>
                            <td>
                                @{{user.phone}}
                            </td>
                            <td>
                                Interests:
                            </td>
                            <td>
                                <ul>
                                    <li v-for="interest in user.interests">
                                        @{{interest.name}}
                                    </li>
                                </ul>
                            </td>
                            <td>
                                Picture:
                            </td>
                            <td>

                                <img v-bind:src="user.picture"/>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection
