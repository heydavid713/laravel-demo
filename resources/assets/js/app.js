/**
 * First we will load all of this project's JavaScript dependencies which
 * include Vue and Vue Resource. This gives a great starting point for
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

Vue.component('example', require('./components/Example.vue'));

const app = new Vue({
    el: '#app',
    data: {interests: [], name: "", email: "", phone: "", picture: "", users: []},
    created: function () {
        var self = this;
        $.get({
            url: '/api/user', dataType: 'json', success: function (data) {
                self.users = data.data;
            }
        });
    },
    methods: {
        createUser: function (event) {

            var form = new FormData();
            form.append('name', this.name);
            form.append('phone', this.phone);
            form.append('email', this.email);
            form.append('interests', [this.interests]);
            form.append('picture', this.picture);

            this.$http.post('/api/user', form, function (response) {
                window.location = '/';
            });
            // if (this.valid) {

            // }
        },
        onFileChange: function (event) {
            this.picture = event.target.files[0] || event.dataTransfer.files[0];
        },
        fetchUsers: function () {
            $.get({
                url: '/api/user', success: function (data) {
                    var self = this;

                    self.users = data;
                }
            });
        }
    }, computed: {
        valid: function () {
            return this.interests.length > 0 && this.name.length > 0 && this.picture.length > 0 && this.email.length > 0 && this.phone.length > 0;
        }
    }
});
