<?php
/**
 * Created by PhpStorm.
 * User: david
 * Date: 10/27/16
 * Time: 5:53 PM
 */

namespace App\Http\Controllers\Web;


use App\Http\Controllers\Controller;
use App\Models\Interest;
use App\Models\Transformers\UserTransformer;
use Illuminate\Http\Request;

class UserController extends Controller
{
    public function index()
    {
        return view('home');
    }

    public function create()
    {
        return view('create', ['interests' => Interest::all()]);
    }
}