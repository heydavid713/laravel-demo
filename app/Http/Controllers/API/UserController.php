<?php
/**
 * Created by PhpStorm.
 * User: david
 * Date: 10/27/16
 * Time: 5:53 PM
 */

namespace App\Http\Controllers\API;


use App\Http\Controllers\Controller;
use App\Models\Interest;
use App\Models\Transformers\UserTransformer;
use App\Models\UserInterests;
use Illuminate\Http\Request;

class UserController extends Controller
{
    public function index(Request $request)
    {
        return fractal()->collection(\App\Models\User::all(), new UserTransformer())->toArray();
    }

    public function store(Request $request)
    {
        $this->validate($request, ['name' => 'required',
            'picture' => 'required|image|dimensions:max_width=600,max_height=800',
            'email' => 'email|required',
            'phone' => 'required',
            'interests' => 'required']);

        $picture = $request->file('picture');

        $user = \App\Models\User::create($request->only(['name', 'email', 'phone', 'interests']) + ['password' => bcrypt('1234'), 'picture' => 'display-pictures/' . $picture->getClientOriginalName()]);
        $picture->move(public_path('display-pictures'), $picture->getClientOriginalName());

        foreach (explode(",", $request->interests) as $interest_id) {
            UserInterests::create(['user_id' => $user->id, 'interest_id' => $interest_id]);
        }

        return fractal()->item($user, new UserTransformer())->toArray();
    }
}