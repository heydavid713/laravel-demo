<?php
/**
 * Created by PhpStorm.
 * User: david
 * Date: 10/27/16
 * Time: 6:02 PM
 */

namespace App\Models\Transformers;


use App\Models\User;
use League\Fractal\TransformerAbstract;

class UserTransformer extends TransformerAbstract
{
    public function transform(User $user)
    {
        return [
            'name' => $user->name,
            'email' => $user->email,
            'picture' => $user->getPicture(),
            'phone' => $user->phone,
            'interests' => $user->interests
        ];
    }
}