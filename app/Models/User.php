<?php

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'picture'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function roles()
    {
        return $this->hasManyThrough(Role::class, UserRoles::class);
    }

    public function interests()
    {
        return $this->hasMany(UserInterests::class);
    }

    public function getInterestsAttribute()
    {
        return Interest::findMany($this->interests()->get()->pluck('interest_id'));
    }

    public function getPicture()
    {
        return asset($this->picture);
    }
}
